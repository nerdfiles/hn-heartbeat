from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from rest_framework.urlpatterns import format_suffix_patterns
from frontend.views import HackerDetail, HackerAdd

from django.contrib import admin
admin.autodiscover()

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),)

urlpatterns += patterns('',
                       url(r'^$', 'frontend.views.HomeView', name='home'),
                       url(r'^dashboard/', include(admin.site.urls)),
                       url(r'^dashboard.links/$',
                           'frontend.views.all_urls_view', name='dashboard-links'),
                       )

urlpatterns += patterns('frontend.views',
                        url(r'^api/hacker/$',
                            HackerAdd.as_view(), name='hacker-create'),
                        url(r'^api/hacker/(?P<username>[\w]+)/$',
                            HackerDetail.as_view(), name='hacker-detail'),)

urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json', 'api'])

#urlpatterns += static(settings.MEDIA_URL,
                      #document_root=settings.MEDIA_ROOT)

# Imagestore config
urlpatterns = patterns(
    '',
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
) + urlpatterns + static(settings.MEDIA_URL, document_root=settings.STATIC_ROOT)

urlpatterns = patterns(
    '',
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT, 'show_indexes': True}),
) + urlpatterns + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

