# Filename: msgbus.coffee
###

                  _
                 | |
 ____   ___  ____| |__  _   _  ___
|    \ /___)/ _  |  _ \| | | |/___)
| | | |___ ( (_| | |_) ) |_| |___ |
|_|_|_(___/ \___ |____/|____/(___/
           (_____|

###
# @description msgbus COMM pattern to higher orders of application interfaces 
#              which should make modular patterns more easily shareable across 
#              multi-platform deployment scenarios.
#
# 1. requests as newable executions which might enable AMD loading
# 2. commands as namespaced executions
# 3. channels as context decorators of executions
# 4. events as triggerable executions

define ["backbone.wreqr"], (Wreqr) ->
  reqres: new Wreqr.RequestResponse()
  commands : new Wreqr.Commands()
  events: new Wreqr.EventAggregator()
  #channels: Wreqr.radio.channel
